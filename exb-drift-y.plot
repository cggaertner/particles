set term svg
set output ARG2
set title 'E×B drift for B_z = 0.1 T, E_x = 10 V/m'
set xlabel 't / s'
set ylabel 'y / m'
set xrange [0:1.31189e-006]
set yrange [:1e-5]
plot ARG1 u 1:3 with lines t 'electron', \
     ARG1 u 1:6 with lines t 'proton'
