set term svg
set output ARG2
set title 'particle in magnetic mirror (configuration 1)'
set xlabel 't / s'
set ylabel 'µ / JT^{-1}'
plot ARG1 u 1:8 w lines t 'magnetic moment'
