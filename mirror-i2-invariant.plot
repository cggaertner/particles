set term svg
set output ARG2
set title 'particle in magnetic mirror (configuration 2)'
set xlabel 't / s'
set ylabel "J' / Js"
plot ARG1 u 1:9 w lines t 'accumulating longitudinal invariant'
