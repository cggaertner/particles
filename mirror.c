#include "boris.h"
#include "parameters.h"

#include <math.h>
#include <stdio.h>

enum { X, Y, Z };
typedef struct boris_particle particle;
typedef struct { double total, para, perp; } energy;

static double dot(const double *u, const double *v)
{
    return u[X]*v[X] + u[Y]*v[Y] + u[Z]*v[Z];
}

static void eval_B(double *B, double B_0, double b, const double *r)
{
    B[X] = -b/2 * r[X];
    B[Y] = -b/2 * r[Y];
    B[Z] = B_0 + b * r[Z];
}

// also computes energy components
static double eval_mu(energy *W, double m, const double *v, const double *B)
{
    double B2 = dot(B, B),
           v2 = dot(v, v),
           v_dot_B = dot(v, B),
           v2_para = v_dot_B * v_dot_B / B2,
           v2_perp = v2 - v2_para;

    W->total = m/2 * v2;
    W->para = m/2 * v2_para;
    W->perp = m/2 * v2_perp;

    return W->perp / sqrt(B2);
}

int main(int argc, char *argv[])
{
    if(argc < 2) {
        fputs(
            "Usage: mirror <PARAMETER_FILE>...\n\n"
            "Parameters:\n"
            "  m      particle mass\n"
            "  q      particle charge\n"
            "  B_0    magnetic field in z direction at origin\n"
            "  b      magnetic field gradient in z direction\n"
            "  x_0    initial position in x direction\n"
            "  v_y    initial velocity in y direction\n"
            "  v_z    initial velocity in z direction\n"
            "  dt     time step size\n"
            "  T      total time\n",
            stderr);
        return 1;
    }

    parameters_loadall(argv + 1);

    double m = parameters_get("m"),
           q = parameters_get("q"),
           B_0 = parameters_get("B_0"),
           b = parameters_get("b"),
           x_0 = parameters_get("x_0"),
           v_y = parameters_get("v_y"),
           v_z = parameters_get("v_z"),
           dt = parameters_get("dt"),
           T = parameters_get("T");

    particle p = { q/m, .r[X] = x_0, .v[Y] = v_y, .v[Z] = v_z };

    double B[3];
    eval_B(B, B_0, b, p.r);

    energy W;
    double mu_0 = eval_mu(&W, m, p.v, B),
           W_0 = W.total,
           J = 0,
           phi_0 = atan2(p.r[Y], p.r[X]);

    printf(
        "# t x y z W_para W_perp W mu J phi\n"
        "0 %g 0 0 %g %g %g %g 0 %g\n",
        x_0, W.para, W.perp, W_0, mu_0, phi_0);

    double t = 0;
    for(int i = 0; (t = i * dt) < T; ++i) {
        boris_next(&p, 0, B, dt);
        eval_B(B, B_0, b, p.r);

        double mu = eval_mu(&W, m, p.v, B);
        J += 2 * W.para * copysign(dt, dot(p.v, B));

        double phi = atan2(p.r[Y], p.r[X]);

        printf("%g %g %g %g %g %g %g %g %g %g\n",
            t, p.r[X], p.r[Y], p.r[Z], W.para, W.perp, W.total, mu, J, phi);
    }

    double dW_rel = fabs(W.total - W_0) / W_0;

    fprintf(stderr,
        "# magnetic mirror [%s]\n"
        "W_0     %g\n"
        "dW_rel  %g\n",
        argv[1], W_0, dW_rel);

    return 0;
}
