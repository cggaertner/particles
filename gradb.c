#include "boris.h"
#include "parameters.h"

#include <math.h>
#include <stdio.h>

#define ELEMENTARY_CHARGE 1.6021766208e-19

enum { X, Y, Z };
typedef struct boris_particle particle;

int main(int argc, char *argv[])
{
    if(argc < 2) {
        fputs(
            "Usage: gradb <PARAMETER_FILE>...\n\n"
            "Parameters:\n"
            "  m      particle mass\n"
            "  Z      charge number\n"
            "  B_0    magnetic field in z direction at origin\n"
            "  grad_B magnetic field gradient in x direction\n"
            "  v_y    initial velocity perpendicular to both B and grad B\n"
            "  dt     time step size\n"
            "  T      total time\n",
            stderr);
        return 1;
    }

    parameters_loadall(argv + 1);

    double m = parameters_get("m"),
           q = parameters_get("Z") * ELEMENTARY_CHARGE,
           B_0 = parameters_get("B_0"),
           grad_B = parameters_get("grad_B"),
           v_y = parameters_get("v_y"),
           dt = parameters_get("dt"),
           T = parameters_get("T");

    double W_0 = m/2 * v_y * v_y;

    particle p = { q/m, .v[Y] = v_y };
    double B[3] = { [Z] = B_0 };

    puts("# t x y z\n0 0 0 0");

    double t = 0;
    for(int i = 1; (t = i * dt) < T; ++i) {
        boris_next(&p, 0, B, dt);
        B[Z] = B_0 + grad_B * p.r[X];
        printf("%g %g %g %g\n", t, p.r[X], p.r[Y], p.r[Z]);
    }

    double W_1 = m/2 * (p.v[X]*p.v[X] + p.v[Y]*p.v[Y] + p.v[Z]*p.v[Z]),
           dW_rel = fabs(W_1 - W_0) / W_0;

    fprintf(stderr,
        "# grad B drift [%s]\n"
        "W_0    %g\n"
        "dW_rel %g\n",
        argv[1], W_0, dW_rel);

    return 0;
}
