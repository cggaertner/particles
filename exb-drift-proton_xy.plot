set term svg size 640, 240
set output ARG2
set title 'proton trajectory for B_z = 0.1 T, E_x = 10 V/m'
set key off
set xlabel 'y / m'
set ylabel 'x / m'
set xrange [-1.4e-004:0]
set yrange [:0]
set size ratio -1
plot ARG1 u 6:5 with lines
