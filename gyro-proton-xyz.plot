set term svg
set output ARG2
set ylabel 'x / m'
set zlabel 'y / m'
set xlabel 'z / m'
set ytics autofreq 5e-5
set ztics autofreq 5e-5
set xtics autofreq 5e-5
set view equal xyz
splot ARG1 u 4:2:3 with lines \
    title "proton\nB = 0.1 T\nv_{y0} = 400 m/s\nv_{z0} = 25 m/s"
