set term svg
set output ARG2
set title 'particle in magnetic mirror (configuration 3)'
set xlabel 't / s'
set ylabel 'W / J'
set yrange [0:8]
plot ARG1 u 1:5 w lines t 'parallel energy', \
     ARG1 u 1:6 w lines t 'perpendicular energy', \
     ARG1 u 1:7 w lines t 'total energy', \
