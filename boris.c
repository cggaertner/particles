#include "boris.h"

#define DIM 3
#define dim(VAR) int VAR = 0; VAR < DIM; ++VAR

enum { X, Y, Z };

typedef struct boris_particle particle;

static const double ZERO[DIM];

static void cross(double *u, const double *v, const double *w)
{
    u[X] = v[Y] * w[Z] - w[Y] * v[Z];
    u[Y] = v[Z] * w[X] - w[Z] * v[X];
    u[Z] = v[X] * w[Y] - w[X] * v[Y];
}

void boris_update_velocity(particle *p,
    const double *E, const double *B, double dt)
{
    if(!E) E = ZERO;
    if(!B) B = ZERO;

    double v_plus[DIM], v_minus[DIM], v_prime[DIM];
    double s[DIM], t[DIM];

    double q_prime = p->q_over_m * dt/2;

    // [t] = q' [B]
    for(dim(i)) t[i] = q_prime * B[i];

    // |t|² = |[t]|²
    double t_mag2 = 0;
    for(dim(i)) t_mag2 += t[i] * t[i];

    // [s] = 2 [t] / (1 + |t|²)
    for(dim(i)) s[i] = 2 * t[i] / (1 + t_mag2);

    // [v-] = [v(n - 1/2)] + q' [E]
    for(dim(i)) v_minus[i] = p->v[i] + q_prime * E[i];

    // [v'] = [v-] + [v-]x[t]
    double v_minus_x_t[DIM];
    cross(v_minus_x_t, v_minus, t);
    for(dim(i)) v_prime[i] = v_minus[i] + v_minus_x_t[i];

    // [v+] = [v-] + [v']x[s]
    double v_prime_x_s[DIM];
    cross(v_prime_x_s, v_prime, s);
    for(dim(i)) v_plus[i] = v_minus[i] + v_prime_x_s[i];

    // [v(n + 1/2)] = [v+] + q' [E]
    for(dim(i)) p->v[i] = v_plus[i] + q_prime * E[i];
}

void boris_push_particle(particle *p, double dt)
{
    for(dim(i)) p->r[i] += p->v[i] * dt;
}

void boris_next(particle *p, const double *E, const double *B, double dt)
{
    boris_update_velocity(p, E, B, dt);
    boris_push_particle(p, dt);
}
