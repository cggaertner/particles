set term svg size 640,240
set output ARG2
set title 'electron trajectory for B_0 = 1 T, ∇_xB = 10^4 T/m, v_{y0} = 10 km/s, T = 0.02 ns'
set key off
set xlabel 'y / m'
set ylabel 'x / m'
set xrange [-15.5e-7:1e-7]
set yrange [-15e-8:5e-8]
set size ratio -1
T = 0.02e-9
plot ARG1 \
    u (column(1) > T ? 1/0 : column(3)):(column(1) > T ? 1/0 : column(2)) \
    w lines
