#include "boris.h"
#include "parameters.h"

#include <math.h>
#include <stdio.h>

#define ELEMENTARY_CHARGE 1.6021766208e-19

enum { X, Y, Z };
typedef struct boris_particle particle;

int main(int argc, char *argv[])
{
    if(argc < 2) {
        fputs(
            "Usage: exb <PARAMETER_FILE>...\n\n"
            "Parameters:\n"
            "  m_1    mass particle 1\n"
            "  m_2    mass particle 2\n"
            "  Z_1    charge number particle 1\n"
            "  Z_2    charge number particle 2\n"
            "  E      electric field in x direction\n"
            "  B      magnetic field in z direction\n"
            "  n      iteration steps per gyration cycle of faster particle\n"
            "  N      number of cycles of slower particle\n",
            stderr);
        return 1;
    }

    parameters_loadall(argv + 1);

    double m_1 = parameters_get("m_1"),
           m_2 = parameters_get("m_2"),
           q_1 = parameters_get("Z_1") * ELEMENTARY_CHARGE,
           q_2 = parameters_get("Z_1") * ELEMENTARY_CHARGE,
           E_x = parameters_get("E"),
           B_z = parameters_get("B"),
           n = parameters_get("n"),
           N = parameters_get("N");

    double v_D = -E_x/B_z;

    double E[3] = { [X] = E_x };
    double B[3] = { [Z] = B_z };

    particle p_1 = { .q_over_m = q_1 / m_1 };
    particle p_2 = { .q_over_m = q_2 / m_2 };

    double T_1 = (2 * M_PI * m_1) / (fabs(q_1) * B_z),
           T_2 = (2 * M_PI * m_2) / (fabs(q_2) * B_z);

    double T_n, T_N;
    if(T_1 <= T_2) T_n = T_1, T_N = T_2;
    else T_n = T_2, T_N = T_1;

    double dt = T_n / n;
    int I = (int)(double)ceil(N * T_N / dt),
        N_g = (int)(double)floor((I * dt) / T_n);

    puts("# t x_1 y_1 z_1 x_2 y_2 z_2 y_rel1 y_rel2 W_1 W_2\n"
        "0 0 0 0 0 0 0 0 0 0 0");

    for(int i = 1; i <= I; ++i) {
        boris_next(&p_1, E, B, dt);
        boris_next(&p_2, E, B, dt);

        double y_D = v_D * i * dt,
               y_rel1 = p_1.r[Y] - y_D,
               y_rel2 = p_2.r[Y] - y_D;

        double W_1 = 0, W_2 = 0;
        for(int j = 0; j < 3; ++j)
            W_1 += p_1.v[j]*p_1.v[j], W_2 += p_2.v[j]*p_2.v[j];

        printf("%.8g %g %g %g %g %g %g %g %g %g %g\n",
            i * dt,
            p_1.r[X], p_1.r[Y], p_1.r[Z],
            p_2.r[X], p_2.r[Y], p_2.r[Z],
            y_rel1, y_rel2, W_1, W_2);
    }

    fprintf(stderr,
        "# ExB drift [%s]\n"
        "T_1 %g\n"
        "T_2 %g\n"
        "v_D %g\n"
        "N_g %i\n",
        argv[1], T_1, T_2, v_D, N_g);

    return 0;
}
