set term svg
set output ARG2
set title 'particle in magnetic mirror (configuration 3)'
set xlabel 't / s'
set ylabel 'φ / 2π'
set yrange [0:0.35]
plot ARG1 u 1:(column(10)/(2*pi)) w lines t 'polar angle'
