set term svg
set output ARG2
set key off
set title 'relative position error (electron)'
set xlabel 't / s'
set ylabel '|Δr| / r_L'
plot ARG1 u 1:8 with lines
