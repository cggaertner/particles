// a particle with specific charge, position and velocity
struct boris_particle
{
    double q_over_m;
    double r[3];
    double v[3];
};

// update particle velocity
// either of the fields E and B may be NULL
void boris_update_velocity(struct boris_particle *p,
    const double *E, const double *B, double dt);

// move particle according to its current velocity
void boris_push_particle(struct boris_particle *p, double dt);

// just calls boris_update_velocity() followed by boris_push_particle()
void boris_next(struct boris_particle *p,
    const double *E, const double *B, double dt);
