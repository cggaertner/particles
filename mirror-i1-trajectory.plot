set term svg
set output ARG2
set title "particle in magnetic mirror\n(configuration 1)"
set xlabel 'x'
set ylabel 'y'
set zlabel 'z'
set xtics ('0' 1)
set ytics ('0' 0)
set ztics ('0' 0)
splot ARG1 u 2:3:4 w lines t "trajectory"
