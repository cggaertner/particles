set term svg
set output ARG2
set title 'drift-relative proton motion for B_z = 0.1 T, E_x = 10 V/m'
set key off
set xlabel 't / s'
set ylabel 'y_{rel} / m'
set xrange [0:1.31189e-006]
plot ARG1 u 1:9 with lines
