<!DOCTYPE html>
<html>
 <head>
  <title>Charged Particles in Static Fields</title>
  <meta charset='utf-8'>
  <script type='text/x-mathjax-config'>
    MathJax.Hub.Config({ tex2jax: { inlineMath: [['$','$']] } });
  </script>
  <script>
    let src = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js';
    let config = 'TeX-AMS-MML_SVG';
    onload = function() {
        let script = document.createElement('script');
        script.src = src + '?config=' + config;
        document.body.appendChild(script);
    };
  </script>
  <style>
    body {
        max-width: 42em;
        margin: 3em auto;
        font-family: sans-serif;
    }
    h1, .center, .figure {
        text-align: center;
    }
    h1 {
        counter-reset: h2-counter;
    }
    h2 {
        counter-increment: h2-counter;
        counter-reset: h3-counter;
        margin-top: 2em;
    }
    h3 {
        counter-increment: h3-counter;
        margin-top: 2em;
    }
    h2:before {
        content: counter(h2-counter) '.\a0\a0';
    }
    h3:before {
        content: counter(h2-counter) '.' counter(h3-counter) '.\a0\a0';
    }
    .bib {
        list-style-type: none;
        counter-reset: bib-counter;
    }
    .bib li {
        margin-bottom: 0.5em;
    }
    .bib li:before {
        content: '[' counter(bib-counter) ']';
        counter-increment: bib-counter;
        display: inline-block;
        margin-left: -2em;
        width: 2em;
    }
    .figure, .abstract, .note {
        font-size: 0.85em;
    }
    .abstract {
        width: 90%;
        margin: 3em auto;
    }
    pre {
        margin-left: 2em;
    }
    @media print {
        body {
            max-width: auto;
            font-size: 10pt;
            font-family: serif;
        }
        .figure img {
            max-width: 80%;
        }
        a {
            color: black;
            text-decoration: none;
        }
        .no-print {
            display: none;
        }
        .no-break, .figure {
            page-break-inside: avoid;
        }
    }
  </style>
 </head>
 <body>

  <h1>Charged Particles in Static Fields</h1>

  <div class='center'>
   Christoph Gärtner<br>
   <small><i>last modified:</i> 2017-07-11</small>
  </div>
  <p class="abstract">Motivated by the single-particle description of plasmas, this project looks at the motion of charged particles in static electromagnetic fields. Gyration, E&times;B and &nabla;B drift will be simulated. Finally, a toy model of a magnetic mirror will be used to demonstrate the adiabatic invariants.</p>

  <h2>Basic Theory</h2>

  <p class="note no-print"><i>Note:</i> The impatient reader may want to skip to either the <a href="#usage">usage information</a> or <a href="#results">my results</a>.</p>
  <p>Depending on field configuration, the Lorentz force</p>
  $$ \vec F = q\left( \vec E + \vec v\times\vec B \right)$$
  <p>will affect particle motion in different ways.</p>

  <h3>Gyration</h3>

  <p>Without an electric field, the equation of motion becomes</p>
  $$ \dot{\vec v} = \frac qm \vec v \times \vec B $$
  <p>Such an acceleration perpendicular to the particle's velocity leads to circular motion around the magnetic field lines, characterized by cyclotron frequency $\omega_c$ and Larmor radius $r_L$ given by</p>
  $$ \omega_c = \frac{|q|B}{m} \qquad r_L = \frac{mv_\perp}{|q|B}$$
  <p>where $v_\perp$ denotes the velocity component perpendicular to the magnetic field.</p>
  <p>Associated with this motion is a magnetic moment</p>
  $$ \mu = \frac{mv_\perp^2}{2B} $$
  <p>proportional to the perpendicular kinetic energy.</p>

  <h3>Guiding Center Drifts</h3>

  <p>When a generic force $\vec F$ is applied to a particle moving in a magnetic field, it experiences a drift perpendicular to both force and field</p>
  $$ \vec v_{F\times B} = \frac 1q \frac{\vec F\times\vec B}{B^2} $$
  <p>which we can interpret as motion of the guiding center of gyration.</p>
  <p>In the special case $\vec F = q\vec E$, the velocity</p>
  $$ \vec v_{E\times B} = \frac{\vec E\times\vec B}{B^2} $$
  <p>becomes independent of the particle's charge: In contrast to other forces such as gravity, the E&times;B drift does not separate opposite charges (but note that given a time-varying electric field, charges will separate under the influence of the polarization drift).</p>
  <p>An example for drifting induced by nonuniform fields is the &nabla;B drift given by</p>
  $$ \vec v_{\nabla B} =\frac{\mu}q \frac{\vec B\times\nabla B}{B^2} $$
  <p>We will not consider any of the other effects due to various field configurations. For a quick overview, consider Wikipedia [<a href="#wiki-gc">1</a>], the slides of professor Melzer's lecture [<a href="#melzer-slides">2</a>] or introductory literature on plasma physics.</p>

  <h3>Magnetic Mirrors</h3>

  <p>Let $\nabla B\parallel\vec B$. Moving towards greater field strength will increase the energy of gyration. As energy is conserved in purely magnetic fields, motion along the field lines will slow.</p>
  <p>Eventually, the parallel kinetic energy will reach zero, and the particle gets reflected. This makes it possible to construct 'magnetic bottles' to trap particles, though it is generally not possible to trap particles with arbitrary initial velocity, yielding a loss cone in velocity space.</p>
  <p>A realistic example for a magnetic mirror is the dipole field of earth, trapping electrons and ions in the magnetosphere.</p>
  <p>A less realistic example is given by a linear increase of field strength in $z$ direction</p>
  $$ B_z = B_0 + bz $$
  <p>with parameters $B_0$, $b$.</p>
  <p>Assuming an isotropic field, Gauß's equation $\nabla\cdot\vec B = 0$ can be used to determine the remaining components</p>
  \begin{align}
  B_x &= -\frac12 bx \\
  B_y &= -\frac12 by
  \end{align}
  <p>This is the field configuration we will investigate.</p>

  <h3>Adiabatic Invariants</h3>

  <p>Given periodic phase space motion, one can derive approximate constants of motion via closed action integrals $\oint p\mathrm dq$ of canonically conjugate variables. These quantities remain constant under slow variations (compared to the integration period) of system parameters and are called adiabatic invariants.</p>
<div class="no-break">
  <p>For particles gyrating in a magnetic field, the first adiabatic invariant is given by</p>
  \begin{align}
   I_1 &amp;= \oint mv_\perp r_L \mathrm d\vartheta \\
       &amp;= 2\pi \frac{m^2v_\perp^2}{|q| B} \\
       &amp;= 4\pi \frac{m}{|q|} \mu
  \end{align}
</div>
  <p>where we integrate the angular momentum of gyration. Up to a constant factor, this is just the magnetic moment.</p>
  <p>If the particle is trapped within the field (eg a particle bouncing between magnetic mirrors), the second adiabatic invariant is given by</p>
  $$ I_2 = \oint mv_\parallel \mathrm ds $$
  <p>and called the longitudinal invariant $J$.</p>
  <p>The drift of the center of this bouncing motion may itself be periodic, giving a third adiabatic invariant</p>
  $$ I_3 = \oint \vec A\cdot \mathrm d\vec s$$
  <p>as an integral over the vector potential $\vec A$, which is the magnetic flux $\Phi$ through the drift surface.</p>

  <h2>Numeric Integration</h2>

  <p>Numeric simulation of these effects can be challenging: The force is velocity dependent, and while a high stability of gyration is important to correctly model physical effects, algorithms also need to be fast as we may need to deal with large sets of particles.</p>

  <h3>Boris Method</h3>

  <p>In 1970, J.P. Boris came up with a scheme that has proven effective. The nomenclature in this section follows [<a href="#brieda-boris">3</a>].</p>
  <p>Starting point is the leapfrog discretization</p>
  $$ \frac{\vec v_{n + 1/2} - \vec v_{n - 1/2}}{\Delta t} = \frac qm \left[\vec E_n + \frac{\vec v_{n - 1/2} + \vec v_{n + 1/2}}2\times\vec B_n \right] $$
  <p>where $\vec E_n = E(\vec r_n)$, $\vec B_n = \vec B(\vec r_n)$.</p>
<div class="no-break">
  <p>Defining $\vec v^{\mathbf -}$ and $\vec v^{\mathbf +}$ as</p>
  \begin{align}
  \vec v^{\mathbf -} &amp;= \vec v_{n - 1/2} + \frac{q\Delta t}{2m} \vec E_n \tag1
  \\
  \vec v^{\mathbf +} &amp;= \vec v_{n + 1/2} - \frac{q\Delta t}{2m} \vec E_n \tag2
  \end{align}
</div>
  <p>makes the explicit dependency on the electric field drop out of the equation, giving</p>
  $$ v^{\mathbf +} - v^{\mathbf -} = \frac{q}{2m} (v^{\mathbf +} + v^{\mathbf -})\times\vec B_n \Delta t $$
  <p>This infinitesimal rotation can be realized as</p>
  $$ \vec v^{\mathbf +} - \vec v^{\mathbf -} = (\vec v^{\mathbf -} + \vec v^{\mathbf -} \times \vec t) \times \vec s \tag3 $$
  <p>where we have introduced vectors</p>
  $$
  \vec t = \frac{q\Delta t}{2m} \vec B_n
  \qquad
  \vec s = \frac{2 \vec t}{1 + |\vec t|^2}
  $$
  <p>Algorithmically, we start by computing $\vec v^{\mathbf -}$ according to (1), followed by the computation of $\vec v^{\mathbf +}$ according to (3).</p>
  <p>The final steps are the computation of $\vec v_{n + 1/2}$ according to (2) followed by advancing the particle according to</p>
  $$ \vec r_{n+1} = \vec r_{n} + \vec v_{n + 1/2} \Delta t $$
  <p>While it is not a variational integrator, the algorithm does conserve phase space volume [<a href="ebq-boris">4</a>] and has good energy stability.</p>

  <h3>Longitudinal Invariant</h3>

  <p>For practical reasons, instead of computing the integral over a full period</p>
  $$ J = \oint mv_\parallel \mathrm ds $$
  <p>we use $v_\parallel \mathrm ds = v_\parallel^2 \mathrm dt$ and track the value</p>
  $$ J'(t) = \int_{t_0}^t mv_\parallel^2 \mathrm d\tau$$
  <p>Furthermore, we weight the integrand with $\mathop{\rm sgn}\vec v\cdot\vec B$ to arrive at the discretization</p>
  $$ J'_k = 2 \sum_{i=0}^k (\mathop{\rm sgn} \vec v\cdot\vec B)\, W_\parallel \, \Delta t$$
  <p>written in terms of the kinetic energy $W_\parallel$ of parallel motion.</p>
  <p>If the adiabatic assumption holds, the weight factor makes $J'_k$ return to its initial value after a full cycle, and we can determine the longitudinal invariant via</p>
  $$ J = 2 \left( \max_k J'_k - \min_k J'_k \right) $$

<div class="no-break">
  <h2>Implementation</h2>

  <p>The Boris method is implemented in <kbd>boris.c</kbd> and its interface documented in <kbd>boris.h</kbd>.</p>
</div>
  <p>Parameter handling is implemented in <kbd>parmeters.c</kbd> and its interface documented in <kbd>parameters.h</kbd>.</p>
  <p>The files <kbd>gyro.c</kbd>, <kbd>exb.c</kbd>, <kbd>gradb.c</kbd> and <kbd>mirror.c</kbd> contain straight-forward particle pushers with main loops that call <code>boris_next()</code>, update the magnetic field according to the new particle position if necessary, and then compute and output the various quantities of interest.</p>

<div class="no-break">
  <h2 id="usage">Usage</h2>

  <p>Build with GNU <kbd>make</kbd>, generate data with the executables <kbd>gyro</kbd>, <kbd>exb</kbd>, <kbd>gradb</kbd> and <kbd>mirror</kbd> and plot it using <kbd>gnuplot</kbd>.</p>
</div>

<div class="no-break">
  <h3>Build</h3>

  <p>The makefile can build the executables as well as generate data files and plots. It supports the following targets:</p>
</div>
  <dl>
   <dt><kbd>plots</kbd></dt>
   <dd>generate <kbd>.svg</kbd> plots <i>(default target, triggers full build)</i></dd>
   <dt><kbd>data</kbd></dt>
   <dd>generate only <kbd>.out</kbd> and <kbd>.err</kbd> data files</dd>
   <dt><kbd>bin</kbd></dt>
   <dd>generate executables, but produce no data or plots</dd>
   <dt><kbd>clean</kbd></dt>
   <dd>remove <kbd>.out</kbd> and <kbd>.err</kbd> files</dd>
   <dt><kbd>realclean</kbd></dt>
   <dd>remove all generated files</dd>
  </dl>
  <p>By default, the build uses the <kbd>clang</kbd> compiler in mode</p>
  <pre>-std=c99 -D_USE_MATH_DEFINES -Weverything -O3</pre>
  <p>Set <kbd>CC=...</kbd> and <kbd>CFLAGS=...</kbd> to adjust this to your own needs.</p>

<div class="no-break">
  <h3>Data Generation</h3>

  <p>Each of the executables can be invoked without parameters to get a list of supported parameters, eg</p>
  <pre>$ ./gyro
Usage: gyro &lt;PARAMETER_FILE&gt;...

Parameters:
  m      particle mass
  Z      charge number
  B      magnetic field in z direction
  v_y    initial velocity perpendicular to B
  v_z    initial velocity parallel to B
  n      interation steps per gyration cycle
  N      number of cycles</pre>
</div>
  <p>If you pass one or more file names as arguments, parameters will be read from these files, where each line should contain the name of a parameter and its numeric value separated by whitespace.</p>
  <p>The main data will be written to standard output, and some further information may be written to standard error. To generate data files, redirect the streams via <kbd>&gt;<i>file</i>.out</kbd> and <kbd>2&gt;<i>file</i>.err</kbd>.</p>

<div class="no-break">
  <h3>Plotting</h3>

  <p>The plots are generated via</p>
  <pre>gnuplot -c <i>script-file</i>.plot <i>input-file</i>.out <i>output-file</i>.svg</pre>
</div>

<div class="no-break">
  <h2 id="results">Results</h2>

  <p>The simulations represent reasonably realistic models of proton and electron motion in case of gyration, E&times;D and &nabla;B drift. In contrast, the magnetic mirror models are parameterized arbitrarily to best showcases the effect under consideration.</p>
</div>

  <h3>Gyration</h3>

  <p>In a field of strength $B=0.1\mathrm T$, protons of initial velocities as specified in figure 1 gyrate with $\omega_c \approx 9.58\times10^6 \mathrm{s}^{-1}$ and $r_L \approx 4.18\times10^{-5} \mathrm{m}$ with a total kinetic energy $W \approx 1.34\times10^{-22} \mathrm{J}$. After $5000$ iteration steps, the relative energy drift was $\sim 6.13\times 10^{-15}$.</p>
  <div class="figure"><img src="gyro-proton-xyz.svg"><br><b>Figure 1:</b> trajectory of gyrating proton</div>
  <p>The maximal deviation from the analytical solution relative to $r_L$ was $\sim 1.25\times 10^{-2}$ at the side of the helix opposite to the starting position, and a final deviation of $\sim 8.27\times 10^{-4}$ at its adjacent side.</p>
  <p>The time evolution of this error is plotted in figure 2.</p>
  <div class="figure"><img src="gyro-proton-error.svg"><br><b>Figure 2:</b> quality of simulated proton trajectory</div>
  <p>Given a proton-electron mass ratio of $\sim 1800$, we increase initial velocities by a factor of $2000$ for electron gyration and end up with $\omega_c \approx 1.76\times 10^{10} \mathrm{s}^{-1}$, $r_L \approx 4.55\times 10^{-5} \mathrm{m}$, $W \approx 2.93\times 10^{-19} \mathrm{J}$ and a final energy drift of $\sim 1.65\times 10^{-16}$.</p>
  <div class="figure"><img src="gyro-electron-xyz.svg"><br><b>Figure 3:</b> trajectory of gyrating electron</div>
  <p>The deviations from the analytical solution were identical to the proton case.</p>

<div class="no-break">
  <h3>E&times;B Drift</h3>

  <p>Let an electric field of strength $E = 10 \mathrm{V}\mathrm{m}^{-1}$ point in $x$-direction and a magnetic field of strength $B = 0.1 \mathrm{T}$ point in $z$-direction and let our particles start out at rest at the origin. We expect and reproduce an E&times;B drift of modulus $v_D = 100 \mathrm{m}\mathrm{s}^{-1}$ pointing in negative $y$-direction for either type of particle.</p>
</div>
  <div class="figure"><img src="exb-drift-y.svg"><br><b>Figure 4:</b> E&times;B drift of electron and proton</div>
  <p>Subtracting the drift, only the gyration remains, exemplarily plotted for the proton in figure 5.</p>
  <div class="figure"><img src="exb-drift-proton_y_rel.svg"><br><b>Figure 5:</b> proton motion relative to drift</div>
  <p>The particle trajectories are cycloids. Due to the difference in masses, figures 6 and 7 needed to be plotted with unequal time scales.</p>
  <div class="figure"><img src="exb-drift-proton_xy.svg"><br><b>Figure 6:</b> proton trajectory at a timescale of $\mathrm{\mu s}$</div>
  <div class="figure"><img src="exb-drift-electron_xy_partial.svg"><br><b>Figure 7:</b> electron trajectory at a timescale of $\mathrm{ns}$</div>
  <p>To assess the quality of the simulation, we plot the $3672$ simulated electron gyrations relativ to the drift in figure 8.</p>
  <div class="figure"><img src="exb-drift-electron_xy_rel.svg"><br><b>Figure 8:</b> electron trajectory relative to drift</div>

<div class="no-break">
  <h3>&nabla;B Drift</h3>

  <p>Let the magnetic field point in $z$-direction with a strength varying according to</p>
  $$ B_z =  1 \mathrm{T} + x\cdot 10^4 \mathrm{T}\mathrm{m}^{-1} $$
</div>
  <p>Furthermore, let the initial particle velocity be $10 \mathrm{km}\mathrm{s}^{-1}$ in $y$-direction, corresponding to a proton energy of $\sim 8.36\times 10^{-20} \mathrm{J}$ and an electron energy of $\sim 4.55\times 10^{-23} \mathrm{J}$. After $500000$ iteration steps corresponding to $500 \mathrm{ns}$, the relative energy shift was $\sim 1.06\times 10^{-13}$ for protons and $\sim 5.36\times 10^{-14}$ for electrons.</p>
  <p>We only look qualitatively at the particle trajectories without further quantitative analysis (figure 9).</p>
  <div class="figure"><img src="gradb-proton-xy_initial.svg"><br><img src="gradb-proton-xy.svg"><br><img src="gradb-electron-xy_initial.svg"><br><img src="gradb-electron-xy.svg"><br><b>Figure 9:</b> proton and electron trajectories at various timescales</div>

<div class="no-break">
  <h3>Magnetic Mirror</h3>

  <p>We use our toy model</p>
  $$
  B_x = -\frac12 bx \qquad
  B_y = -\frac12 by \qquad
  B_z = B_0 + bz
  $$
  <p>Three sets of system parameters were chosen to showcase each of the adiabatic invariants.</p>
  </div>

<div class="no-break">
  <h4>First Adiabatic Invariant</h4>
   <p>The particle is reflected once at a positive $z$ value and escapes towards apparent infinity.</p>
</div>
  <div class="figure"><img src="mirror-i1-trajectory.svg"><br><b>Figure 10:</b> particle trajectory with single reflection</div>
  <p>A look at the kinetic energies shows the expected transfer from parallel to perpendicular motion as well as a high degree of stability of total energy.</p>
  <div class="figure"><img src="mirror-i1-energy.svg"><br><b>Figure 11:</b> single reflection energy balance</div>
  <p>As long as the temporal or spatial characteristics of particle gyration remain small compared to relevant system characteristics, the first invariant can be considered a constant of motion.</p>
  <div class="figure"><img src="mirror-i1-invariant.svg"><br><b>Figure 12:</b> approximate conservation of magnetic moment</div>

<div class="no-break">
  <h4>Second Adiabatic Invariant</h4>
  <p>The chosen field configuration allows entrapment of particles even without an explicit second focal point, as would be present for the magnetic bottle or a dipole field.</p>
</div>
  <div class="figure"><img src="mirror-i2-trajectory.svg"><br><b>Figure 13:</b> trajectory of trapped particle</div>
  <p>The proxy $J'$ for the longitudinal invariant displays the expected periodicity. The corresponding value for $J$ would be $\sim 5.7 \mathrm{J}\mathrm{s}$.</p>
  <div class="figure"><img src="mirror-i2-invariant.svg"><br><b>Figure 14:</b> proxy variable for longitudinal invariant</div>
  <h4>Third Adiabatic Invariant</h4>
  <p>We do not try to determine the value of the third invariant, but merely show the drifting motion necessary for its definition.</p>
  <div class="figure"><img src="mirror-i3-trajectory.svg"><br><b>Figure 15:</b> trajectory of particle with drifting center of bounce</div>
  <p>The polar angle relative to the $x$ axis shows a linear gain. The magnetic flux through the surface enclosed by this motion is invariant under adiabatic system evolution, but in practice, the drift can be too slow to be of interest.</p>
  <div class="figure"><img src="mirror-i3-phi.svg"><br><b>Figure 16:</b> angular drift of trapped particle</div>

<div class="no-break">
  <h2>Literature</h2>

  <ol class="bib">
   <li id="wiki-gc">Wikipedia contributors. <i>Wikipedia: Guiding center</i>. Retrieved 2017-07-06,<br>from <a href="https://en.wikipedia.org/wiki/Guiding_center">https://en.wikipedia.org/wiki/Guiding_center</a>.</li>
   <li id="melzer-slides">A. Melzer. <i>Vorlesungsfolien Plasmaphysik</i>. Retrieved 2017-04-26,<br>from <a href="https://physik.uni-greifswald.de/ag-melzer/lehre/">https://physik.uni-greifswald.de/ag-melzer/lehre/</a>.</li>
   <li id="brieda-boris">L. Brieda. <i>Particle Push in Magnetic Field (Boris Method)</i>. Retrieved 2017-07-05,<br>from <a href="https://www.particleincell.com/2011/vxb-rotation/">https://www.particleincell.com/2011/vxb-rotation/</a>.</li>
   <li id="ebq-boris">Ellison, C. L.; Burby, J. W.; Qin, H. <i>Comment on "Symplectic integration of magnetic systems": A proof that the Boris algorithm is not variational</i>. Journal of Computational Physics, Volume 301, p. 489-493.</li>
  </ol>
</div>
 </body>
</html>
