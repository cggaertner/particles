set term svg
set output ARG2
set title 'drift-relative electron motion for B_z = 0.1 T, E_x = 10 V/m'
set key off
set xlabel 't / s'
set ylabel 'y_{rel} / m'
set xrange [0:7.14478e-010]
plot ARG1 u 1:8 with lines
