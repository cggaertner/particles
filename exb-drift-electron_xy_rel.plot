set term svg size 480, 240
set output ARG2
set title 'drift-relative electron trajectory for B_z = 0.1 T, E_x = 10 V/m'
set key off
set xlabel 'x / m'
set ylabel 'y_{rel} / m'
set size ratio -1
set xtics autofreq 4e-9
set ytics autofreq 2e-9
set xrange [-12.5e-9:1.5e-9]
set yrange [-7e-9:7e-9]
plot ARG1 u 2:8 with lines
