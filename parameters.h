#include <stdio.h>

// check if a parameter has been set
_Bool parameters_has(const char *name);

// get parameter value
double parameters_get(const char *name);

// load parameter file
void parameters_load(const char *file);

// load all files from a NULL-terminated list of strings such as argv
void parameters_loadall(char **args);

// dump loaded parameters to file
void parameters_dump(FILE *fp);
