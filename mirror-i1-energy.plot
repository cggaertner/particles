set term svg
set output ARG2
set title 'particle in magnetic mirror (configuration 1)'
set xlabel 't / s'
set ylabel 'W / J'
set yrange [0:14]
plot ARG1 u 1:5 w lines t 'parallel energy', \
     ARG1 u 1:6 w lines t 'perpendicular energy', \
     ARG1 u 1:7 w lines t 'total energy', \
