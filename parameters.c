#include "parameters.h"

#include <ctype.h>
#include <errno.h>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef PARAMETERS_MAX
#define PARAMETERS_MAX 1024
#endif

#ifndef PARAMETERS_BUFSIZE
#define PARAMETERS_BUFSIZE 65536
#endif

struct parameter
{
    const char *name;
    double value;
};

static struct parameter parameters[PARAMETERS_MAX];
static size_t count;

static char buffer[PARAMETERS_BUFSIZE];
static size_t top;

static size_t remaining(void)
{
    return sizeof buffer - top - 2;
}

static int lookup(const void *key, const void *value)
{
    return strcmp(key, *(char *const *)value);
}

static int compare(const void *a, const void *b)
{
    return strcmp(*(char *const *)a, *(char *const *)b);
}

void parameters_dump(FILE *fp)
{
    for(size_t i = 0; i < count; ++i)
        fprintf(fp, "%s %.17g\n", parameters[i].name, parameters[i].value);
}

_Bool parameters_has(const char *name)
{
    return bsearch(name, parameters, count, sizeof *parameters, lookup);
}

double parameters_get(const char *name)
{
    const struct parameter *para = bsearch(
        name, parameters, count, sizeof *parameters, lookup);

    if(!para) {
        fprintf(stderr, "parameter '%s' not defined\n", name);
        exit(1);
    }

    return para->value;
}

void parameters_load(const char *file)
{
    FILE *fp = fopen(file, "rb");
    if(!fp) {
        perror(file);
        exit(1);
    }

    size_t read = fread(buffer + top, 1, remaining(), fp);
    if(ferror(fp)) {
        perror(file);
        exit(1);
    }

    if(!feof(fp)) {
        fputs("parameter buffer too small", stderr);
        exit(1);
    }

    fclose(fp);

    char *cc = buffer + top;
    for(;;) {
        while(isspace(*cc)) ++cc;
        if(*cc == '#') {
            do ++cc;
            while(*cc && *cc != '\n');
        }

        if(*cc == 0) break;
        if(*cc == '\n') {
            ++cc;
            continue;
        }

        char *name = cc;

        if(!isalpha(*cc)) {
            fprintf(stderr, "illegal character '%c' in '%s'\n", *cc, file);
            exit(1);
        }

        do ++cc;
        while(isgraph(*cc));

        if(!isblank(*cc)) {
            fprintf(stderr, "illegal character '%c' in '%s'\n", *cc, file);
            exit(1);
        }

        *cc = 0;

        do ++cc;
        while(isblank(*cc));

        errno = 0;
        double value = strtod(cc, &cc);
        if(errno) {
            perror(name);
            exit(1);
        }

        if(count == PARAMETERS_MAX) {
            fputs("too many parameters", stderr);
            exit(1);
        }

        parameters[count++] = (struct parameter){ name, value };

        while(isblank(*cc) || *cc == '\r') ++cc;
        if(*cc == 0) break;
        if(*cc != '\n') {
            fprintf(stderr, "illegal character '%c' in '%s'\n", *cc, file);
            exit(1);
        }
    }

    top += read + 1;

    qsort(parameters, count, sizeof *parameters, compare);
    for(size_t i = 1; i < count; ++i) {
        if(strcmp(parameters[i].name, parameters[i-1].name) == 0) {
            fprintf(stderr,
                "redefinition of parameter '%s'\n", parameters[i].name);
            exit(1);
        }
    }
}

void parameters_loadall(char **argp)
{
    for(; *argp; ++argp)
        parameters_load(*argp);
}
