set term svg size 640,240
set output ARG2
set title 'proton trajectory for B_0 = 1 T, ∇_xB = 10^4 T/m, v_{y0} = 10 km/s, T = 20 ns'
set key off
set xlabel 'y / m'
set ylabel 'x / m'
set xrange [-5e-5:105e-5]
set yrange [-5e-5:15e-5]
set ytics autofreq 5e-5
set size ratio -1
T = 20e-9
plot ARG1 \
    u (column(1) > T ? 1/0 : column(3)):(column(1) > T ? 1/0 : column(2)) \
    w lines
