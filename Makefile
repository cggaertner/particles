ifeq ($(origin CC),default)
CC = clang
CFLAGS ?= -std=c99 -D_USE_MATH_DEFINES -Weverything -O3
endif

get-token = $(wordlist $1,$1,$(subst -, ,$2))
base-name = $(call get-token,1,$1)-$(call get-token,2,$1)

BIN = gyro exb gradb mirror
OBJ = boris.o parameters.o
SVG := $(patsubst %.plot,%.svg,$(wildcard *.plot))
OUT := $(sort $(foreach NAME,$(SVG),$(call base-name,$(NAME)).out))
ERR = $(OUT:%.out=%.err)
JUNK = $(OUT) $(ERR)
MOREJUNK = $(BIN) $(OBJ) $(SVG)

plots: $(SVG)
bin: $(BIN)
data: $(OUT)

realclean: JUNK += $(MOREJUNK)
clean realclean:; rm -rf $(JUNK)

plot-list:; @du -h $(sort $(wildcard $(SVG)))
data-list:; @du -h $(sort $(wildcard $(OUT)))

$(BIN): $(OBJ)
$(OBJ): %.o: %.h

$(OUT):
	./$^ >$@ 2>$(@:%.out=%.err)
	-@du -h $@

$(SVG):
	gnuplot -c $^ $@
	-@du -h $@

.SECONDEXPANSION:
$(OUT): %.out: $$(call get-token,1,$$@) %.in
$(SVG): %.svg: %.plot $$(call base-name,$$@).out
