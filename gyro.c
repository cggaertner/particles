#include "boris.h"
#include "parameters.h"

#include <math.h>
#include <stdio.h>

#define ELEMENTARY_CHARGE 1.6021766208e-19

enum { X, Y, Z };
typedef struct boris_particle particle;

// compute deviation from analytic solution relative to Larmor radius
static double rel_error(const particle *p, double r_L, double phi, double z)
{
    double x_0 = -copysign(r_L, p->q_over_m);

    double x = x_0 * cos(phi),
           y = r_L * sin(phi);

    double dx = p->r[X] - x,
           dy = p->r[Y] - y,
           dz = p->r[Z] - z;

    return sqrt(dx*dx + dy*dy + dz*dz) / r_L;
}

int main(int argc, char *argv[])
{
    if(argc < 2) {
        fputs(
            "Usage: gyro <PARAMETER_FILE>...\n\n"
            "Parameters:\n"
            "  m      particle mass\n"
            "  Z      charge number\n"
            "  B      magnetic field in z direction\n"
            "  v_y    initial velocity perpendicular to B\n"
            "  v_z    initial velocity parallel to B\n"
            "  n      interation steps per gyration cycle\n"
            "  N      number of cycles\n",
            stderr);
        return 1;
    }

    parameters_loadall(argv + 1);

    int n = (int)(double)parameters_get("n"),
        N = (int)(double)parameters_get("N");

    double m = parameters_get("m"),
           q = parameters_get("Z") * ELEMENTARY_CHARGE,
           B_z = parameters_get("B"),
           v_y = parameters_get("v_y"),
           v_z = parameters_get("v_z");

    double r_x = -(m * v_y) / (q * B_z);

    double r_L = fabs(r_x),
           T_L = (2 * M_PI * m) / (fabs(q) * B_z),
           omega_c = fabs(q) * B_z / m,
           W_0 = m/2 * (v_y*v_y + v_z*v_z);

    double dt = T_L / n;

    double B[3] = { [Z] = B_z };
    particle p = { q / m, .r[X] = r_x, .v[Y] = v_y, .v[Z] = v_z };

    printf(
        "# t x y z v_x v_y v_z dr_rel\n"
        "%g %g %g %g %g %g %g %g\n",
        0.0,
        p.r[X], p.r[Y], p.r[Z],
        p.v[X], p.v[Y], p.v[Z],
        0.0);

    double dr_rel = 0,
           dr_maxrel = 0;

    for(int i = 1; i <= n * N; ++i) {
        boris_next(&p, 0, B, dt);

        double phi = 2 * M_PI * (i % n) / n,
               z = v_z * i * dt;

        dr_rel = rel_error(&p, r_L, phi, z);
        if(dr_rel > dr_maxrel)
            dr_maxrel = dr_rel;

        printf("%g %g %g %g %g %g %g %g\n",
            i * dt,
            p.r[X], p.r[Y], p.r[Z],
            p.v[X], p.v[Y], p.v[Z],
            dr_rel);
    }

    double W_1 = m/2 * (p.v[X]*p.v[X] + p.v[Y]*p.v[Y] + p.v[Z]*p.v[Z]);
    double dW_rel = fabs(W_1 - W_0) / W_0;

    fprintf(stderr,
        "# gyration [%s]\n"
        "r_L       %g\n"
        "T_L       %g\n"
        "omega_c   %g\n"
        "W_0       %g\n"
        "dW_rel    %g\n"
        "dr_rel    %g\n"
        "dr_maxrel %g\n",
        argv[1], r_L, T_L, omega_c, W_0, dW_rel, dr_rel, dr_maxrel);

    return 0;
}
